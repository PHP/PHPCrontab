<?php

/*

Copyright © Zéfling - 2014 ( http://ikilote.net/fr/Blog/Techno-magis.html )

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

The Software is provided "as is", without warranty of any kind, express or
implied, including but not limited to the warranties of merchantability,
fitness for a particular purpose and noninfringement. In no event shall the
authors or copyright holders be liable for any claim, damages or other liability,
whether in an action of contract, tort or otherwise, arising from, out of or
in connection with the software or the use or other dealings in the Software.

*/

/**
 * Gestion du fichier crontab
 * @author zefling
 * @version 1.0 (2014-06-02)
 * @license Expat
 */
class Exec_Cron {

	const VERSION_CODE     = '1.0';
	const VERSION_CODE_NUM = '1.0 (2014-06-02)';

	private static $cron;

	public static $tmp_file = '/tmp/crontab.txt';

	/**
	 * Recupérer une instance du cron pour l'utilisateur de PHP (ex : www-data)
	 * @return Exec_Cron un instance
	 */
	public static function getCron() {
		if (self::$cron == null) {
			self::$cron = new Exec_Cron ();
		}
		return self::$cron;
	}

	// -------------- CRON --------------------------------
	/**
	* @var array liste des actions
	*/
	private $action = array ();

	/**
	 * le constructeur parse le crontab de l'utilisateur
	*/
	private function __construct() {
		try {
			$this->parse ( shell_exec ( 'crontab -l' ) );
		} catch (Exception $e ) {
			print_r($e);
		}
	}

	/**
	 * vide toutes les informations du crontab de l'utilisateur
	 */
	public function erase () {
		$this->action = array ();
	}

	/**
	 * parse les informations du  crontab de l'utilisateur
	 * @param string $txt le fichier cron
	 * @throws Exception en cas d'erreur de parsing
	 */
	public function parse($txt) {
		$lignes = explode ( PHP_EOL, $txt );

		foreach ( $lignes as $num => $ligne ) {
			if (!empty($ligne)) {
				try {
					$cronTime = Exec_CronTime::parse_string ( $ligne );
					$this->set ( $cronTime, $cronTime->expression );
				} catch (Exception $e)  {
					throw new Exception ( "Error on line {$num} : {$ligne}\nMessage : {$e->getMessage()}");
				}
			}
		}
	}

	/**
	 * rendu d'une ligne de cron
	 * @param Exec_CronTime $time information temporel
	 * @param string $action une action à lancer dans le cron
	 * @return string ligne de cron
	 */
	private function show(Exec_CronTime $time, $action) {
		return (( string ) $time) . ' ' . $action;
	}

	/**
	 * ajouter une ligne dans le cron
	 * @param CronTime $time information temporel
	 * @param string $action une action à lancer dans le cron
	 * @param string $id de la ligne (si null, un crc de l'action servira d'id ; par défaut : null)
	 * @return $id (si l'id est null, un id est généré);
	 */
	public function set(Exec_CronTime $time, $action, $id = null) {
		$hash = empty ( $id ) ? 'crc-' . crc32 ( $action ) : $id;
		$this->action [$hash] = array (
				'rule' => $time,
				'action' => $action
		);

		return $hash;
	}

	/**
	 * supprime une action (si identique à une action présente dans la liste)
	 * @param string $action une action à supprimer du cron
	 */
	public function removByAction($action) {
		unset ( $this->action ['crc-' . crc32 ( $action )] );
	}

	/**
	 * supprime une action avec son id (ne fonction pas sur les lignes parsées)
	 * @param string $action une action à supprimer du cron
	 */
	public function removeById($id) {
		unset ( $this->action [$id] );
	}

	/**
	 * récupérer une action par son action (pas franchement intéressant, mais possible)
	 * @param string $action une action à supprimer du cron
	 * @return array('rule' => Exec_CronTime , 'action' => String)
	 */
	public function getByAction($action) {
		return $this->action ['crc-' . crc32 ( $action )];
	}

	/**
	 * récupérer une action avec son id (ne fonction pas sur les lignes parsées)
	 * @param string $action une action à supprimer du cron
	 * @return array('rule' => Exec_CronTime , 'action' => String)
	 */
	public function getById($id) {
		return $this->action [$id];
	}

	/**
	 * met à jours Crontab en passant par une fichier temporaire.
	 */
	public function updateCrontab() {
		$tmp = self::$tmp_file;
		file_put_contents ( $tmp , $this->__toString() );
		echo exec ( "crontab {$tmp}" );
	}

	/**
	 * Pour générer le contenu du fichier Crontab.
	 * @return string le contenu généré
	 */
	public function __toString() {
		$content = "";
		foreach ( $this->action as $action ) {
			$content .= $this->show ( $action ['rule'], $action ['action'] ) . PHP_EOL;
		}
		return $content;
	}
}
