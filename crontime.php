<?php 
/*

Copyright © Zéfling - 2014 ( http://ikilote.net/fr/Blog/Techno-magis.html )

Permission is hereby granted, free of charge, to any person obtaining 
a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

The Software is provided "as is", without warranty of any kind, express or 
implied, including but not limited to the warranties of merchantability, 
fitness for a particular purpose and noninfringement. In no event shall the 
authors or copyright holders be liable for any claim, damages or other liability, 
whether in an action of contract, tort or otherwise, arising from, out of or 
in connection with the software or the use or other dealings in the Software.

*/

/**
 * Gestion du temps pour une commande Crontab
 * @author zefling
 * @version 1.0.1 (2014-06-09)
 * @license Expat
 * 
 * TODO ajouter support des noms pour les mois et jours de la semaines ?
 * TODO BSD n'autorise pas le multi liste type 1-2,5-6 ?
 */
class Exec_CronTime {

	const VERSION_CODE     = '1.0';
	const VERSION_CODE_NUM = '1.0.1 (2014-06-09)';
	
	/** type : minutes (0-60) */
	const TYPE_MINUTES       = 1;
	/** type : heures (0-23) */
	const TYPE_HOURS         = 2;
	/** type : jour du mois (1-31) */
	const TYPE_DAYS_OF_MONTH = 3;
	/** type : mois (1-12) */
	const TYPE_MONTHS        = 4;
	/** type : jour de la semaine (0-7, 0 et 7 = dimanche) */
	const TYPE_DAYS_OF_WEEK  = 5;

	/** Au démarre */
	const SP_REBOOT  = '@reboot';
	/** Tous les ans : 0 0 1 1 * */
	const SP_YEARLY  = '@yearly';
	/** Tous les mois : 0 0 1 * * */
	const SP_MONTHLY = '@monthly';
	/** Toutes les semaines : 0 0 * * 0 */
	const SP_WEEKLY  = '@weekly';
	/** TTous les jours : 0 0 * * * */
	const SP_DAILY   = '@daily';
	/** Toutes les heures : 0 * * * * */
	const SP_HOURLY  = '@hourly';
	
	public static $special = array(
		'reboot'   => null, 
		'yearly'   => '0 0 1 1 *', 
		'annually' => '0 0 1 1 *', 
		'monthly'  => '0 0 1 * *', 
		'weekly'   => '0 0 * * 0', 
		'daily'    => '0 0 * * *', 
		'midnight' => '0 0 * * *', 
		'hourly'   => '0 * * * *'
	);  

	/**
	 * @var array liste des données par groupe de temps
	 */
	protected $data = null;
	
	/**
	 *  @var string expression (utilisé uniquement au parsing)
	 */
	public $expression = null;

	/**
	 * ajout d'une donnée
	 * @param int $type type de temps (exemple : Exec_CronTime::TYPE_MINUTES)
	 * @param int|array $data donnée à ajouter. Si c'est un tableau, il doit contenir : ('start','end') et/ou 'cycle' 
	 * @param boolean $replace  si vrai remplacer les données existantes, si faux ajouts aux donnée existes (par défaut : true)
	 */
	public function set($type, $data, $replace = true) {
		
		if (!is_array($this->data)) {
			$this->data = array (
				self::TYPE_MINUTES       => array(),
				self::TYPE_HOURS         => array(),
				self::TYPE_DAYS_OF_MONTH => array(),
				self::TYPE_MONTHS        => array(),
				self::TYPE_DAYS_OF_WEEK  => array()
			);
		}
		
		if ($data == '*') {
			$this->data [$type] = array();
		} 
		elseif ($this->test($type, $data) && isset ( $this->data [$type] )) {
			if ($replace) {
				$this->data[$type] = array ( $data );
			} else {
				$this->data[$type][] = $data;
			}
		} else {			
			throw new Exception ("invalide data" );
		}
	}
	
	public function set_special ($name) {
		if (!empty($name) && $name[0] == '@' && array_key_exists(substr($name, 1), self::$special)) {
			$this->data = $name;
		}
	} 
	
	/**
	 * teste d'une donnée
	 * @param int $type type de temps (exemple : Exec_CronTime::TYPE_MINUTES)
	 * @param int|array $data donnée à tester. Si c'est un tableau, il doit contenir : ('start','end') et/ou 'cycle'
	 */
	public function test($type, $data) {
		$test = true;
		if (is_array($data)) {
			if (isset($data['start']) && isset($data['end'])) {
				$test = $this->test_num($type, $data['start']) && $this->test_num($type, $data['end'])
					&& $data['start'] < $data['end'];
			}
			if ($test && isset($data['cycle']) && !$this->test_num($type, $data['cycle'])) {
				$test = false;
			}
		} else {
			$test = $this->test_num($type, $data);
		}
		return $test;
	}
	
	public function test_num ($type, $num) {
		switch ($type) {
			case self::TYPE_MINUTES       : return $num >= 0 && $num <  60;
			case self::TYPE_HOURS         : return $num >= 0 && $num <  24;
			case self::TYPE_DAYS_OF_MONTH : return $num >= 1 && $num <= 31;
			case self::TYPE_MONTHS        : return $num >= 1 && $num <= 12;
			case self::TYPE_DAYS_OF_WEEK  : return $num >= 0 && $num <=  7;
		}
		return false;
	}
	
	/**
	 * ajout une donnée d'intervale (ex : 1-5 ce qui est équivalent à 1,2,3,4,5)
	 * @param int $type type de temps (exemple : Exec_CronTime::TYPE_MINUTES)
	 * @param int $start valeur de début
	 * @param int $end valeur de fin
	 * @param boolean $replace si vrai remplacer les données existantes, si faux ajouts aux donnée existes (par défaut : true)
	 */
	public function set_interval($type, $start, $end, $replace = true) {
		$this->set ( $type, array('start' => $start, 'end' => $end), $replace );
	}
	
	/**TYPE_DAYS_OF_MONTH
	 * ajout une donnée de pas pour des intervales réguliers (ex : * /5 ce qui est équivalent à 5,10,15,...)
	 * @param int $type type de temps (exemple : Exec_CronTime::TYPE_MINUTES)
	 * @param int $size_cycle valeur de le durée d'un cycle 
	 * @param boolean $replace si vrai remplacer les données existantes, si faux ajouts aux donnée existes (par défaut : true)
	 */
	public function set_every($type, $size_cycle, $replace = true) {
		$this->set ( $type, array('cycle' => $size_cycle), $replace );
	}
	/**
	 * ajout une donnée de pas pour des intervales réguliers sur une intervale de temps
	 * (ex : 10-20/5 ce qui est équivalent à 10,15,20)
	 * @param int $type type de temps (exemple : Exec_CronTime::TYPE_MINUTES)
	 * @param int $start valeur de début 
	 * @param int $end valeur de fin 
	 * @param int $size_cycle valeur de le durée d'un cycle 
	 * @param boolean $replace si vrai remplacer les données existantes, si faux ajouts aux donnée existes (par défaut : true)
	 */
	public function set_interval_every($type, $start, $end, $size_cycle, $replace = true) {
		$this->set ( $type, array('start' => $start, 'end' => $end , 'cycle' => $size_cycle), $replace );
	}
	
	/**
	 * à tout les cycles (*)
	 * @param int $type type de temps (exemple : Exec_CronTime::TYPE_MINUTES)
	 */
	public function set_all($type) {
		$this->set ( $type, '*');
	}
	
	/**
	 * Retourne la donnée de temps (ex. : 0 0-12/3 * * *)
	 * @return string la donnée de temps pour cronTab
	 */
	public function __toString() {
		if ($this->data != null) {
			$data = $this->data;
			if (is_array($data)) {
				foreach ($data as &$line) {
					foreach ($line as &$elem ) {
					
						if (is_array($elem)) {
							
							$rendu = null;
							
							// cas intervale
							if (isset($elem['start']) && isset($elem['end'])) {
								$rendu = $elem['start'].'-'.$elem['end'];
							}
							// cas intervale + cycle
							if (isset($elem['start']) && isset($elem['end']) && isset($elem['cycle'])) {
								$rendu .= '/'.$elem['cycle'];
							}
							// cas cycle 
							if ($rendu == null && isset($elem['cycle'])) {
								$rendu = '*/'.$elem['cycle'];
							}
		
							if ($rendu == null) {
								throw new Exception ("invalide data, render impossible" );
							}
							
							$elem = $rendu;
						} 
						elseif (!is_numeric($elem)) {
							throw new Exception ("invalide data, render impossible" );
						}
					}
					
					$tmp = implode ( ',', $line );
					$line = empty ( $tmp ) && $tmp === "" ? '*' : $tmp;
				}
				return implode ( ' ', $data );
			} else {
				return $data;
			}
		}
		return null;
	}
	
	/**
	 * Parse une ligne de cron pour remplire les données CronTime
	 * @param string $string ligne de cron (Ex. : 0 0-12/3 * * * echo 'test' > ~/text.txt)
	 * @throws Exception si une erreur de parsing est trouvée
	 * @return Exec_CronTime un objet peuplé avec les informations du cron
	 */
	public static function parse_string($string) {
		
		// Note  (\s*([0-9*\-,/]+)){5}\s*(.*) ne fonctionne pas car seul le dernier groupe est capturé, il faut tous les écrire 
		
		if (preg_match ('/\s*(?<time>(?<minute>[0-9*\-,\/]+)\s+(?<hour>[0-9*\-,\/]+)\s+(?<day>[0-9*\-,\/]+)\s+'.
				        '(?<month>[0-9*\-,\/]+)\s+(?<weekday>[0-9*\-,\/]+)|(?<special>@[a-z]*))\s*(?<command>.*)/', 
				        $string, $match )
		) {
			$cronTime = new Exec_CronTime();

			if (!empty($match['command'])) { 
				$cronTime->expression = $match['command'];
			}
			
			if (!empty($match['special'])) {
				$cronTime->set_special($match['special']);
			} 
			else {
				foreach ( $match as $pos => &$part ) {
					
					if (is_numeric($pos) && $pos >= 2 && $pos <= 6) {
						foreach ( explode ( ',', $part ) as $elem ) {
							
							if (preg_match ( '/((?<a>(?<a1>[0-9]+)-(?<a2>[0-9]+))|(?<b>[0-9]+)|(?<c>\*))(\/(?<p>[0-9]+))?/', 
									$elem, $match_elem )) {
								
								if (isset ( $match_elem ['p'] )) {
									if (! empty ( $match_elem ['a'] )) {
										$cronTime->set_interval_every ( $pos - 1, $match_elem ['a1'], $match_elem ['a2'], $match_elem ['p'], false );
									} elseif (! empty ( $match_elem ['b'] ) || $match_elem ['b'] == '0') {
										throw new Exception ( "Invalide expression “{$match[0]}” on “{$elem}”" );
									} elseif (! empty ( $match_elem ['c'] )) {
										$cronTime->set_every ( $pos - 1, $match_elem ['p'], false );
									}
								} else {
									if (! empty ( $match_elem ['a'] )) {
										$cronTime->set_interval ( $pos - 1, $match_elem ['a1'], $match_elem ['a2'], false );
									} elseif (! empty ( $match_elem ['b'] ) || $match_elem ['b'] === '0') {
										$cronTime->set ( $pos - 1, $match_elem ['b'], false );
									}
								}
							} else {
								throw new Exception ( "Invalide expression “{$match[0]}” on “{$elem}”" );
							}
						}
					}
				}
			}
			
			return $cronTime;
		}
		
		throw new Exception ( "Invalide expression “{$string}”" );
	}
}
